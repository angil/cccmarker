
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * s1.java
 * 
 * @author Jamie Langille
 * @version Date: Mar.01/2011 Comments added: Mar.03/2011
 */

public class s5 {
	public static void main(String[] args) throws IOException {
		
		/*
		 * I almost had this one, tried to fix it, and it got worse. This
		 * question wasn't even marked.
		 * 
		 * Seriously, get Kevin to do this one-he got 6/7 cases correct.
		 */

		Scanner in = new Scanner(new File("s5.in"));
		int k = in.nextInt();
		boolean a[] = new boolean[k+1];
		for (int i = 0; i < k; i++) {
			a[i] = in.next().charAt(0) == '1';
		}
		a[k] = false;
		
		/*
		 * Count how many lights you have to add while... umm.. MESS OF CODE >.<
		 * 
		 * Note: this is what happens to your code when you start to panic.
		 * Compare this to any of the previous questions. Also, when this
		 * happens, and you are out of time, print a random (or constant)
		 * number from [1,9].
		 */
		int count = 0;
		LOOP : while (true) {
			
			int start = -1, end = -1, longest = -1, ts = -1, te = -1;
			
			// find longest run
			for (int i = 0; i < a.length; i++) {
				if (a[i]) {
					ts = i;
					boolean cond = true;
					for (int j = i + 1; j < a.length; j++) {
						if (!a[j]) {
							if (cond) {
								cond = false;
								te = j;
							} else {
								int length = j - ts;
								if (longest < length) {
									longest = length;
									start = ts;
									end = j;
								}
								i += te - ts - 1;
								break;
							}
						}
					}
					int length = a.length - ts;
					if (longest < length) {
						longest = length;
						start = ts;
						end = a.length;
					}
					i += te - ts - 1;
				} /*else if (i < a.length - 1 && a[i + 1]) {
					ts = i;
					for (int j = i + 1; j < a.length; j++) {
						if (!a[j]) {
							int length = j - ts;
							if (longest < length) {
								longest = length;
								start = ts;
								end = j;
							}
							break;
						}
					}
					int length = a.length - ts;
					if (longest < length) {
						longest = length;
						start = ts;
						end = a.length;
					}
				}*/
			}
			
			// turn one on (arbitrary)
			count++;
			
			// none left to find
			if (longest < 0)
				break LOOP;
			
			if (longest < 4) {
				for (int i = start; i < end; i++) {
					if (!a[i]) {
						a[i] = true;
						break;
					}
				}
			} else {
				for (int i = start; i < end; i++) {
					a[i] = false;
				}
			}
			
			for (int i = 0; i < a.length; i++) {
				System.out.print(a[i] ? "1" : "0");
			}
			System.out.println();
		}
		
		// fail
		System.out.println(count);
		in.close();
	}
}
