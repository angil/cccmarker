
import java.io.IOException;
import java.util.Scanner;

/**
 * j1.java
 * 
 * @author Jamie Langille
 * @version Date: Mar.02/2011 Comments added: Mar.03/2011
 */

public class j1 {
	public static void main(String[] args) throws IOException {
		
		/*
		 * console input: # of antennae (a) # of eyes (e)
		 */
		Scanner in = new Scanner(System.in);
		System.out.println("How many antennas?");
		int a = in.nextInt();
		System.out.println("How Many Eyes?");
		int e = in.nextInt();
		
		// check and print for each type
		if (a >= 3 && e <= 4)
			System.out.println("TroyMartian");
		if (a <= 6 && e >= 2)
			System.out.println("VladSaturnian");
		if (a <= 2 && e <= 3)
			System.out.println("GraemeMercurian");
		
		in.close();
	}
}
