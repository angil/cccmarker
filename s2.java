
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * s1.java
 * 
 * @author Jamie Langille
 * @version Date: Mar.01/2011 Comments added: Mar.03/2011
 */

public class s2 {
	public static void main(String[] args) throws IOException {
		
		Scanner in = new Scanner(new File("s2.in"));
		int n = in.nextInt();
		char[] ans = new char[n];
		char[] que = new char[n];
		
		/*
		 * reading these in I made them the same case, just to be sure. I don't
		 * think it makes any difference, and .toUpperCase() would be more
		 * efficient, blah blah blah
		 */
		for (int i = 0; i < n; i++) {
			ans[i] = in.next().toLowerCase().charAt(0);
		}
		for (int i = 0; i < n; i++) {
			que[i] = in.next().toLowerCase().charAt(0);
		}
		
		// count how many are right (match)
		int count = 0;
		for (int i = 0; i < que.length; i++) {
			if (que[i] == ans[i])
				count++;
		}
		System.out.println(count);
		in.close();
	}
}
