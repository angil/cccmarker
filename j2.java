
import java.io.IOException;
import java.util.Scanner;

/**
 * j2.java
 * 
 * @author Jamie Langille
 * @version Date: Mar.02/2011 Comments added: Mar.03/2011
 */

public class j2 {
	public static void main(String[] args) throws IOException {
		
		/*
		 * console input: humidity (h) max. # of hours (m)
		 */
		Scanner in = new Scanner(System.in);
		int h = in.nextInt();
		int m = in.nextInt();
		
		/*
		 * start t at 1 (its on the ground at 0 no matter what), and continue
		 * until you reach m
		 */
		for (int t = 1; t <= m; t++) {
			
			// ran out of time
			if (t == m)
				System.out.println("The baloon does not touch ground in the given time.");
			
			/*
			 * calculate 'a' based on the given formula try to avoid using a
			 * built-in 'Math.pow()' function wherever possible i.e. replace
			 * Math.pow(t, 2) with t*t
			 * 
			 * Note: casting to an int works: Math.pow using integer parameters
			 * will always return an integer equivalent.
			 */
			int a = (int) (-6 * Math.pow(t, 4) + h * Math.pow(t, 3) + 2 * t * t + t);
			
			// hit the ground-print the time and quit the loop
			if (a <= 0) {
				System.out.printf("The baloon first touches ground at hour:%n%s%n", t);
				break; // quits loop
			}
		}
		in.close();
	}
}
