
import java.io.IOException;
import java.util.Scanner;

/**
 * j3.java
 * 
 * @author Jamie Langille
 * @version Date: Mar.02/2011 Comments added: Mar.03/2011
 */

public class j3 {
	public static void main(String[] args) throws IOException {
		
		/**
		 * console input: t1, the first term t2, the second term
		 */
		Scanner in = new Scanner(System.in);
		int t1 = in.nextInt();
		int t2 = in.nextInt();
		
		// count the number of terms (2 so far)
		int count = 2;
		
		/*
		 * until t2 > t1, find the difference and re-assign t1 and t2
		 * 
		 * Note: make sure you have a temporary variable as a place holder
		 * between 'moving' t1 and t2, or data will be lost
		 */
		for (int temp = 0; t2 < t1; count++) {
			temp = t2;
			t2 = t1 - t2;
			t1 = temp;
		}
		
		System.out.println(count);
		in.close();
	}
}
