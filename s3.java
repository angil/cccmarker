import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * s3.java
 * 
 * @author Jamie Langille
 * @version Date: Mar.01/2011 Comments added: Mar.03/2011
 */

public class s3 {
	
	// oh yes: delicious.
	public static Map<Point, Boolean> map;
	
	// I actually got one test case (or 1/6th of a case) wrong with this answer,
	// perhaps someone could help me find the error? Probably something to do
	// with rounding...idk
	public static void main(String[] args) throws IOException {

		Scanner in = new Scanner(new File("s3.in"));
		int t = in.nextInt();
		
		/**
		 * Okay. Take a deep breath:
		 * 
		 * This I used to describe the basic crystal structure (on the 5*5 grid,
		 * m = 1). if a point isn't here, there is no crystal. If false, that
		 * means crystal is there. If true, that means there <i>could</i> be
		 * crystal there. We can't tell at the current magnification (or if we
		 * can, it isn't there).
		 */
		Map<Point, Boolean> map = new HashMap<Point, Boolean>();
		map.put(new Point(2, 1), false);
		map.put(new Point(2, 0), false);
		map.put(new Point(3, 0), false);
		map.put(new Point(1, 0), false);
		map.put(new Point(2, 2), true);
		map.put(new Point(1, 1), true);
		map.put(new Point(3, 1), true);
		
		/*
		 * Input: t the number of test cases. For t cases, input magnification
		 * factor (m), x, and y.
		 */
		for (int rep = 0; rep < t; rep++) {
			int m = in.nextInt();
			int x = in.nextInt();
			int y = in.nextInt();
			
			// print if there would be crystal or not. Very easy style to debug
			System.out.println(recurse(map, m, x, y) ? "crystal" : "empty");
		}
		in.close();
	}
	
	/**
	 * Uses recursion in regards to the magnification level, starting at the
	 * largest magnification and 'zooming up'. This is hard to picture, but wait
	 * for it.
	 * 
	 * You don't need to use recursion, but if I were to change it it would have
	 * taken too long during the competition. Besides, it will only recurse 13
	 * times max. So, its just as good.
	 * 
	 * In hindsight, I think I did this backwards... but somehow got almost all
	 * of the cases correct... hmm...
	 * 
	 * @param map
	 *            A map describing the 'basic' crystal structure
	 * @param m
	 *            The specified magnification level
	 * @param x
	 *            The x-coordinate to look in
	 * @param y
	 *            The y-coordinate to look in
	 * @return true if crystal, false otherwise (see what I did there?)
	 */
	private static boolean recurse(Map<Point, Boolean> map, int m, int x, int y) {
		
		// calculate the current dimension(s) based on the magnification
		int pow = (int) Math.pow(5, m - 1); //*// inefficient use array
		
		/*
		 * integer division to find the 'equivalent' x and y coordinates for an
		 * 'equivalent' 5x5 grid (basically, grouping the 5^m*5^m, sized grid
		 * into a 5*5 grid)
		 */
		int dx = x / pow;
		int dy = y / pow;
		
		// using the map, find if there is crystal, or not, or maybe
		// should have removed the 'else's. Whatever.
		if (!map.containsKey(new Point(dx, dy)))
			return false;
		if (map.get(new Point(dx, dy)) == false)
			return true;
		
		// here we could have crystal:
		else {
			/*
			 * if we are at the maximum magnification (trust me, this is max),
			 * there "wouldn't" be crystal. Otherwise, subsection the next part
			 * of grid.
			 */
			if (m == 1)
				return false;
			else {
				return recurse(map, m - 1, x % pow, y % pow);
			}
		}
	}
}
