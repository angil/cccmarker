
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * s1.java
 * 
 * @author Jamie Langille
 * @version Date: Mar.01/2011 Comments added: Mar.03/2011
 */

public class s4 {
	public static void main(String[] args) throws IOException {
		
		Scanner in = new Scanner(new File("s4.in"));
		int donors[] = new int[8];
		int accept[] = new int[8];
		for (int i = 0; i < donors.length; i++) {
			donors[i] = in.nextInt();
		}
		for (int i = 0; i < accept.length; i++) {
			accept[i] = in.nextInt();
		}
		
		// keep a running count on how much blood has been donated
		int count = 0;
		
		// for each acceptor...
		for (int i = 0; i < accept.length; i++) {
			
			// run-through possible donors
			for (int j = i; j >= 0 && accept[i] > 0; j--) {
				
				// ignore donors you can't get from
				// Rh- can't accept from Rh+ and types A and B are incompatible
				if (donors[j] <= 0 || (i % 2 == 0 && j % 2 == 1)
						|| ((j == 2 || j == 3) && (i == 4 || i == 5))
						|| ((j == 4 || j == 5) && (i == 2 || i == 3)))
					continue;
				
				// take the max. difference between #donors and #acceptors from
				// both donors and acceptors, and add it to count
				int d = 0;
				if (accept[i] > donors[j]) {
					d = donors[j];
				} else {
					d = accept[i];
				}
				donors[j] -= d;
				accept[i] -= d;
				count += d;
			}
		}
		
		// Ta-Da!!
		System.out.println(count);
		in.close();
	}
}
