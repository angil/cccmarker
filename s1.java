
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * s1.java
 * 
 * @author Jamie Langille
 * @version Date: Mar.01/2011 Comments added: Mar.03/2011
 */

public class s1 {
	public static void main(String[] args) throws IOException {
		
		Scanner in = new Scanner(new File("s1.in"));
		int n = Integer.parseInt(in.nextLine());
		
		// Count the number of 't's and 's's (ignore case)
		int ns = 0;
		int nt = 0;
		for (int i = 0; i < n; i++) {
			String next = in.nextLine();
			next = next.toLowerCase();
			for (int j = 0; j < next.length(); j++) {
				if (next.charAt(j) == 's') {
					ns++;
				}
				if (next.charAt(j) == 't') {
					nt++;
				}
			}
		}
		
		// if #ts > #ss, its english, otherwise its french
		// Note: if #ts == #ss, its french, as in the question (READ CAREFULLY)
		System.out.println((nt > ns) ? "English" : "French");
		in.close();
	}
}
