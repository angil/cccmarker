--[[s3.lua
	Jamie Langille
	May 1 / 2011
	Still doesn't work
	]]

--Setup the map
map={}
map[0] = {x = 2, y = 1, v = false}
map[1] = {x = 2, y = 0, v = false}
map[2] = {x = 3, y = 0, v = false}
map[3] = {x = 1, y = 0, v = false}
map[4] = {x = 2, y = 2, v = true}
map[5] = {x = 1, y = 1, v = true}
map[6] = {x = 3, y = 1, v = true}

--Recursive method for lookup
function recurse(m, x, y)

	pow = math.floor(math.pow(5, m - 1))
	dx, dy = math.floor(x / pow), math.floor(y / pow)
	c = -1

	for i = 0, 6 do
		--Tricky...maps in lua (kind of)
		if (map[i]["x"] == x and map[i]["y"] == y) then
			c = i
			break
		end
	end

	if (c == -1) then
		return false
	elseif (map[c]["v"] == false) then
		return true
	elseif (m == 1) then
			return false
	else
		return recurse(m - 1, math.mod(x, pow), math.mod(y, pow))
	end
end

--Input from file
io.input("s3.in")

--# of test cases
t = io.read("*n")
for rep = 1, t do

	--Get magnification, x, and y
	m, x, y = io.read("*n", "*n", "*n")
	print("m:" .. m, "x:" .. x, "y:" .. y)
	--Print answer
	print(recurse(m, x, y) and "crystal" or "empty")

end

io.input():close()

--testing
print(recurse(1, 2, 1))
