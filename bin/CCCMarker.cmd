@echo off
color 0b
cls

:: Uses g++ and javac if not already on path
path=%path%;C:\Dev-C>\bin;C:\Program Files\Java\jdk1.6.0_19\bin;Lua\5.1

:: Normal Execution
java -jar CCCMarker.jar
pause

:: Run to file
::java -jar CCCMarker.jar > CCC.out
::timeout 5
::start CCC.out

::Cleanup
taskkill /f /im conhost.exe /t
taskkill /f /im cmd.exe /t