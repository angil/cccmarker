#include<string>
#include<iostream>
#include<fstream>
using namespace std;
int main (int argc, char* argv[]){
    ifstream fin ("s4.in");
    
    int donors[8];
    int accept[8];
    for (int i = 0; i < 8; i++)
        fin >> donors[i];
    for (int i = 0; i < 8; i++)
        fin >> accept[i];
    
    int count = 0;
    
    for (int i = 0; i < 8; i++){
        for (int j = i; j >= 0; j--){
            if (donors[j] <= 0 || (i % 2 == 0 && j % 2 == 1)
						|| ((j == 2 || j == 3) && (i == 4 || i == 5))
						|| ((j == 4 || j == 5) && (i == 2 || i == 3)))
					continue;
				int d = 0;
				if (accept[i] > donors[j]) {
					d = donors[j];
				} else {
					d = accept[i];
				}
				donors[j] -= d;
				accept[i] -= d;
				count += d;    
        }
    }
    
    cout << count << endl;
    
    fin.close();
    system("pause");
    return 0;
}
