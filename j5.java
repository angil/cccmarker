
import java.io.IOException;
import java.util.Scanner;

/**
 * j5.java
 * 
 * @author Jamie Langille
 * @version Date: Mar.02/2011 Comments added: Mar.03/2011
 */

public class j5 {
	
	// I hate Node problems, so naturally I didn't want to do it.
	// I can do it later if you want, or we can just talk about it.
	public static void main(String[] args) throws IOException {
		Scanner in = new Scanner(System.in);
		
		// code goes here
		
		in.close();
	}
	
	// or possibly here (likely both)
}
