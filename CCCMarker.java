
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class CCCMarker {
	
	public static void main(String[] args) throws IOException, InterruptedException {
		
		// Overall process: finds all directories to mark
		// Assumed projects are all in the root directory of the drive (i.e. a
		// USB)
		// Assumed all test data in the directory 'data'
		
		Process p = Runtime.getRuntime().exec("cmd /c dir");
		Scanner pin = new Scanner(p.getInputStream());
		
		for (int i = 0; i < 5; i++)
			pin.nextLine();
		
		System.out.println("CCC Marking Program");
		System.out.println("By: Jamie Langille (2011)");
		System.out.println();
		
		while (pin.hasNext()) {
			
			if (pin.next().equals("<DIR>")) {
				
				String dir = pin.next();
				if (dir.contains(".") || !dir.contains("CCC"))
					continue;
				System.out.printf("Marking \'%s\'...%n", dir);
				
				Process type = Runtime.getRuntime().exec("cmd /c dir /b " + dir
						+ "\\src\\s1.*");
				type.waitFor();
				
				// find the language utilised
				Scanner tin = new Scanner(type.getInputStream());
				String lang = tin.next().substring(3);
				
				try {
					while (lang.equals("in") || lang.equals("out") || lang.equals("txt"))
						lang = tin.next().substring(3);
				} catch (NoSuchElementException e){
					System.out.println("Sorry, this language isn't supported.");
					continue;
				}
				tin.close();
				
				// marks questions
				PROG: for (int i = 1; i <= 5; i++) {
					System.out.printf("s%d.%s...%n", i, lang);
					Process runner = null;
					try {
						
						// modifies the original code to read each test case
						// maintains integrity of original code: i.e. writes to
						// input\s1.[lang]
						Scanner fin = new Scanner(new File(dir + "\\src\\s" + i
								+ "." + lang));
						PrintWriter fout = new PrintWriter(new FileWriter("data\\s"
								+ i + "." + lang));
						String line = "";
						while (!(line = fin.nextLine()).contains("\"s" + i
								+ ".in\""))
							fout.println(line);
						
						if (lang.equals("java")){
							line = line.substring(0, line.indexOf("\"s" + i + ".in\""))
							+ "args[0]" + line.substring(line.indexOf("\"s" + i + ".in\"") + 7);
						} else if (lang.equals("lua")){
							line = line.substring(0, line.indexOf("\"s" + i + ".in\""))
							+ "arg[1]" + line.substring(line.indexOf("\"s" + i + ".in\"") + 7);
						} else if (lang.equals("cpp")){
							line = line.substring(0, line.indexOf("\"s" + i + ".in\""))
							+ "argv[1]" + line.substring(line.indexOf("\"s" + i + ".in\"") + 7);
						}
						
						fout.println(line);
						while (fin.hasNext())
							fout.println(fin.nextLine());
						fin.close();
						fout.close();
						
						// compiles the program
						if (lang.equals("java"))
							runner = Runtime.getRuntime().exec("cmd /c javac data\\s" + i + ".java");
						else if (lang.equals("cpp"))
							//g++ s4.cpp -o s4.exe
							runner = Runtime.getRuntime().exec("cmd /c g++ data\\s" + i + ".cpp -o data\\s" + i + ".exe");
						else if (lang.equals("lua"))
							runner = Runtime.getRuntime().exec("cmd /c echo Lua doesn't need to be compiled =P");
						runner.waitFor();
					} catch (Exception e){
						System.out.println("                  E");
						continue PROG;
					}
					
					// marks each test case
					int right = (i == 5) ? 10 : 5;
					CASE: for (int j = 1; j <= ((i == 5) ? 10 : 5); j++) {
						
						try{
							// runs the file
							if (lang.equals("java"))
								runner = Runtime.getRuntime().exec("cmd /c java -cp data s"
										+ i + " data\\s" + i + "." + j + ".in");
							else if (lang.equals("cpp"))
								runner = Runtime.getRuntime().exec("cmd /c data\\s"
										+ i + ".exe data\\s" + i + "." + j + ".in");
							else if (lang.equals("lua"))
								runner = Runtime.getRuntime().exec("cmd /c lua data\\s" + i + ".lua data\\s" + i + "." + j + ".in");
							else {
								System.out.println("                  E");
								continue PROG;
							}
							
							long now = System.currentTimeMillis();
							long timeoutInMillis = 1000L * 10;
							long finish = now + timeoutInMillis;
							while (isAlive(runner) && (System.currentTimeMillis() < finish)){
								Thread.sleep(10);
							}
							if (isAlive(runner)){
								System.out.print("T");
								right--;
								if (lang.equals("cpp"))
									Runtime.getRuntime().exec("cmd /c taskkill /f /im s" + i + ".exe /t");
								Thread.sleep(500);
								continue CASE;
							}
							
							
							Scanner rin = new Scanner(runner.getInputStream());
							Scanner rout = new Scanner(new File("data\\s" + i + "."
									+ j + ".out"));
							
							// displays marking scheme
							boolean correct = true;
							while (rout.hasNext() && rin.hasNext()) {
								if (!rin.hasNext()) {
									correct = false;
									right = Math.max(0, --right);
									break;
								} else if (!rout.hasNext()) {
									correct = false;
									right = Math.max(0, --right);
									break;
								} else if (!rout.nextLine().equals(rin.nextLine())) {
									correct = false;
									right = Math.max(0, --right);
									break;
								}
							}
							rin.close();
							rout.close();
							System.out.print((correct) ? "." : "x");
						} catch (Exception e){
							System.out.print("E");
							right = 0;
						}
					}
					System.out.printf("\t%s%d/%d%n", (i == 5) ? "" : "\t", right, (i == 5) ? 10 : 5);
				}
				System.out.println();
			}
		}
		pin.close();
		
		// delete all created files
		Runtime.getRuntime().exec("cmd /c del data\\s*.cpp");
		Runtime.getRuntime().exec("cmd /c del data\\s*.exe");
		Runtime.getRuntime().exec("cmd /c del data\\s*.lua");
		Runtime.getRuntime().exec("cmd /c del data\\s*.java");
		Runtime.getRuntime().exec("cmd /c del data\\s*.class");
		System.out.println("Done marking.");
	}
	
	public static boolean isAlive(Process p){
		try {
			p.exitValue();
			return false;
		} catch (IllegalThreadStateException e) {
			return true;
		}
	} 
}