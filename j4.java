
import java.awt.Point;
import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * j4.java
 * 
 * @author Jamie Langille
 * @version Date: Mar.02/2011 Comments added: Mar.03/2011
 */

public class j4 {
	public static void main(String[] args) throws IOException {
		
		// console input
		Scanner in = new Scanner(System.in);
		
		/*
		 * I used a set for all points that are already part of the 'path'
		 */
		Set<Point> set = new HashSet<Point>();
		set.add(new Point(0, -1));
		set.add(new Point(0, -2));
		set.add(new Point(0, -3));
		set.add(new Point(1, -3));
		set.add(new Point(2, -3));
		set.add(new Point(3, -3));
		set.add(new Point(3, -4));
		set.add(new Point(3, -5));
		set.add(new Point(4, -5));
		set.add(new Point(5, -5));
		set.add(new Point(5, -4));
		set.add(new Point(5, -3));
		set.add(new Point(6, -3));
		set.add(new Point(7, -3));
		set.add(new Point(7, -4));
		set.add(new Point(7, -5));
		set.add(new Point(7, -6));
		set.add(new Point(7, -7));
		set.add(new Point(6, -7));
		set.add(new Point(5, -7));
		set.add(new Point(4, -7));
		set.add(new Point(3, -7));
		set.add(new Point(2, -7));
		set.add(new Point(1, -7));
		set.add(new Point(0, -7));
		set.add(new Point(-1, -7));
		set.add(new Point(-1, -6));
		set.add(new Point(-1, -5));
		
		// keep track of the last point the drill was at
		Point last = new Point(-1, -5);
		
		// get the first set of commands
		char q = in.next().charAt(0);
		int n = in.nextInt();
		
		// there is no danger so far
		boolean danger = false;
		
		// until the command to quit is given...
		// Note: this is the 'LOOP'
		LOOP : while (q != 'q') {
			
			/*
			 * 4 cases for command 'q' where we will do something:
			 * 
			 * for each of the respective directions, move the last point one
			 * unit in the given direction as long as the set does not contain
			 * the next point.
			 * 
			 * If we do encounter danger, still move the point, set danger to
			 * true, and quit 'LOOP'
			 */
			switch (q) {
				case 'l' : {
					for (int i = 0; i < n; i++) {
						last.translate(-1, 0);
						if (set.contains(last)) {
							danger = true;
							break LOOP; // quits 'LOOP'
						}
					}
					break;
				}
				case 'd' : {
					for (int i = 0; i < n; i++) {
						last.translate(0, -1);
						if (set.contains(last)) {
							danger = true;
							break LOOP; // quits 'LOOP'
						}
					}
					break;
				}
				case 'r' : {
					for (int i = 0; i < n; i++) {
						last.translate(1, 0);
						if (set.contains(last)) {
							danger = true;
							break LOOP; // quits 'LOOP'
						}
					}
					break;
				}
				case 'u' : {
					for (int i = 0; i < n; i++) {
						last.translate(0, 1);
						if (set.contains(last)) {
							danger = true;
							break LOOP; // quits 'LOOP'
						}
					}
					break;
				}
			}
			
			/*
			 * if this code is reached, we fulfilled the command without any
			 * danger: display progress and get next command
			 */
			System.out.println(last.x + " " + last.y + " safe");
			q = in.next().charAt(0);
			n = in.nextInt();
		}
		
		// print "DANGER" only if danger == true, otherwise print "" (nothing
		// visible to the marker)
		System.out.println(danger ? last.x + " " + last.y + " DANGER" : "");
		
		in.close();
	}
}
